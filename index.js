const mailgun = require("mailgun-js");
const apiKey = process.env.MAILGUN_API_KEY;
const DOMAIN = process.env.MAILGUN_DOMAIN;
const MAILGUN_ENDPOINT = process.env.MAILGUN_ENDPOINT;
const MAILGUN_HOST = process.env.MAILGUN_HOST;

const { server, app } = require('./generateServer')();
const mailgunConfig = {
    apiKey,
    domain: DOMAIN,
}
if (MAILGUN_ENDPOINT) {
    mailgunConfig.endpoint = MAILGUN_ENDPOINT;
}

if (MAILGUN_HOST) {
  mailgunConfig.host = MAILGUN_HOST;
}

const mg = mailgun(mailgunConfig);

app.get('/health', (req, res) => {
    // Check mailgun stats
    mg.get(`/${DOMAIN}/stats/total`, { event: [ 'accepted', 'delivered']})
    .then(stats => {
        return res.status(200).send('OK')
    })
    .catch(err => {
        console.log(err);
        return res.status(503).send('MAILGUN IS DOWN');
    })
})

app.post('/', (req, res) => {
    const {
        from,
        to,
        subject,
        html,
        text,
    } = req.body;

    if (!from || !to || !subject || (!html && !text)) {
        return res.status(400).send('Invalid input, required fields from|to|subject|html||text')
    }
    const mailOptions = {
        from,
        to,
        subject,
    }
    if (html) {
        mailOptions.html = html;
    } else {
        mailOptions.text = text;
    }


    mg.messages().send(mailOptions, function (err, body) {
        if (err) {
            console.log('error sending email', err);
            return res.status(400).send('Something went wrong')
        }
        console.log('Sent email', body)
        return res.json({ success: true });
    })

})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
